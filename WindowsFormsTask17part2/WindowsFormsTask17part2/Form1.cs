﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task17;
using System.Data.SQLite;
using System.IO;

namespace WindowsFormsTask17part2
{

    public partial class Form1 : Form
    {
        List<Character> ListofCharacters = new List<Character>();
        string selectedname1 = "name1";
        string selectedclass1 = "defaultc";
        string insertline = "";
        int class1 = 0;

        public Form1()
        {
            InitializeComponent();
            LoadListofCharacter();
        }
        private void LoadListofCharacter()
        {
            
        }


        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            selectedname1 = textBox1.Text;
        }

        private void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass1 = "Warrior";
            class1 = 1;
        }

        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass1 = "Thief";
            class1 = 2;
        }

        private void RadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass1 = "White Wizard";
            class1 = 3;
        }

        private void RadioButton4_CheckedChanged(object sender, EventArgs e)
        {
            selectedclass1 = "Grey Wizard";
            class1 = 4;

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Button2_Click_1(object sender, EventArgs e)
        {
            Character c = null;
            // Create a caracter based on the selected class with the selectedname
            if (class1 == 1)
            {
                c = new Warrior(selectedname1, 1000, 100, 10);
            }
            else if (class1 == 2)
            {
                c = new Thief(selectedname1, 1000, 100, 10);
            }
            else if (class1 == 3)
            {
                c = new WhiteWizard(selectedname1, 1000, 100, 10);
            }
            else if (class1 == 4)
            {
                c = new GreyWizard(selectedname1, 1000, 100, 10);
            }
            else
            {
                MessageBox.Show("Select a class");
            }
            // Print summary
            MessageBox.Show(c.PrintSummary());

            // Add a row to the table with IDCharacter

            using (SQLiteConnection conn = CreateConnection())
            {
                SQLiteCommand sqlite_cmd;

                sqlite_cmd = conn.CreateCommand();
                if (class1 == 1)
                {
                    insertline = $"INSERT INTO IDCharacterTable (Name,Energy,Healthpoints,Armorrating,Class,Subclass) VALUES('{selectedname1}', 1000, 100, 10, 'Warrior', 'Warrior');";
                }
                else if (class1 == 2)
                {
                    insertline = $"INSERT INTO IDCharacterTable (Name,Energy,Healthpoints,Armorrating,Class,Subclass) VALUES('{selectedname1}', 1000, 100, 10, 'Theif', 'Thief');";
                }
                else if (class1 == 3)
                {
                    insertline = $"INSERT INTO IDCharacterTable (Name,Energy,Healthpoints,Armorrating,Class,Subclass) VALUES('{selectedname1}', 1000, 100, 10, 'Wizard', 'WhiteWizard');";
                }
                else if (class1 == 4)
                {
                    insertline = $"INSERT INTO IDCharacterTable (Name,Energy,Healthpoints,Armorrating,Class,Subclass) VALUES('{selectedname1}', 1000, 100, 10, 'Wizard', 'GreyWizard');";
                }
                else
                {
                    MessageBox.Show("select a class");
                }
                sqlite_cmd.CommandText = insertline;
                sqlite_cmd.ExecuteNonQuery();
            }

            //Write to file
            using (StreamWriter sw = new StreamWriter("Character.txt"))
            {
                sw.Write(c.PrintSummary());
            }
            using (StreamWriter sw = new StreamWriter("CharacterInsertSQL.txt"))
            {
                sw.Write(insertline);
            }
        }

        static SQLiteConnection CreateConnection()
        {

            SQLiteConnection sqlite_conn;
            // Create a new database connection:

            sqlite_conn = new SQLiteConnection("Data Source=Task17IDCharacter.db; Version = 3; New = True; Compress = True; ");

            // Open the connection:
            try
            {

                sqlite_conn.Open();

            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message);
            }

            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }

            return sqlite_conn;
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            // The user should have selcted a Character from the CharacterList
            // Use select and print out a message with info about the selected Character

            using (SQLiteConnection conn = CreateConnection())
            {

                SQLiteDataReader sqlite_datareader;

                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = "SELECT * FROM IDCharacterTable";

                sqlite_datareader = sqlite_cmd.ExecuteReader();

                while (sqlite_datareader.Read())
                {
                    string myreader = sqlite_datareader.GetInt32(0) + " " + sqlite_datareader.GetString(1) + " " + sqlite_datareader.GetString(2) + " " + sqlite_datareader.GetString(3) + " " + sqlite_datareader.GetInt32(4) + " " + sqlite_datareader.GetInt32(5) + " " + sqlite_datareader.GetInt32(6);
                    MessageBox.Show(myreader);
                    listBox2.Items.Add(myreader);
                }
            }
        }

        private void Button4_Click_1(object sender, EventArgs e)
        {
            using (SQLiteConnection conn = CreateConnection())
            {

                SQLiteDataReader sqlite_datareader;

                SQLiteCommand sqlite_cmd;
                sqlite_cmd = conn.CreateCommand();
                sqlite_cmd.CommandText = "DELETE FROM IDCharacterTable";

                sqlite_datareader = sqlite_cmd.ExecuteReader();
                MessageBox.Show("All Character has been deleted from the list");
                //Clear the list
                listBox2.Items.Clear();

            }
        }

        private void ListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}


